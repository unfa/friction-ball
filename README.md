# Friction Ball

unfa's (no longer) secret ball game.

## [play the NEW version (Godot 3 based)](https://gotm.io/unfa-games/friction-ball) on GOTM.io

#### [play the old version (Godot 3 based)](https://unfa.xyz/s/Pei9zees) on unfa.xyz

If you want more, rejoice!
[There's a spiritual successor in the works!](https://mastodon.social/@unfa/111246096507413389)