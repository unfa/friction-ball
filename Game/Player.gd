extends RigidBody3D

@export var speed := 5.0

@export var velocity_cap : float = 100

var phys_time_prev : int

var time_of_last_collision_usec : int = 10000

var pos_previous := Vector3.ZERO
var rot_previous := Vector3.ZERO

var velocity := 0.0
var avg_velocity := 0.0

var avg_fps := 60.0

var avg_spin := 0.0

var previous_collisions : int = 0

var mobile := true
#var previous_velocity : float = 0.0

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
#func _ready():
#	if OS.has_feature("mobile"):
#		mobile = true
#	elif OS.has_feature("arm"):
#		mobile = true
#	elif OS.has_feature("arm64"):
#		mobile = true
#	elif OS.has_feature("Android"):
#		mobile = true
#	elif OS.has_feature("iOS"):
#		mobile = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _input(event):
	if Input.is_action_just_pressed("reset"):
		mobile = false
		$Reset.play()
		reset()
		$Spin.play()
		$"../CanvasLayer/Help".show()
		$"../CanvasLayer/Help".modulate = Color.WHITE

func optimize():
#	avg_fps = lerp(avg_fps, Performance.get_monitor(Performance.TIME_FPS), 0.2)

	#avg_fps = 10

	#print("avg_fps: ", avg_fps)

#	if mobile:
#		$Dust.hide()
#		$Trail.hide()
#		$"../WorldEnvironment".environment.glow_enabled = false
#		vport.shadow_atlas_size = 0
#
#		vport.size.x *= 0.33333333
#		vport.size.y = OS.window_size.y * (vport.size.x / OS.window_size.x)
#
#		$Timer.paused = true
#
#	else:
	var fps = Engine.get_frames_per_second()
	var fps_lerp = 0.005 if fps > avg_fps else 0.5

	avg_fps = lerp(avg_fps, fps, fps_lerp)

	var vport : Viewport = get_viewport()

	if avg_fps < 35:
		if $Dust.visible:
			$Dust.hide()
			avg_fps += 15
		elif $"../WorldEnvironment".environment.glow_enabled:
			$"../WorldEnvironment".environment.glow_enabled = false
			avg_fps += 15
		elif $Trail.visible:
			$Trail.hide()
			avg_fps += 15
		elif $"../DirectionalLight3D".shadow_enabled:
			$"../DirectionalLight3D".shadow_enabled = false
			avg_fps += 15
		elif vport.scaling_3d_scale > 0.15:
			move_toward(vport.scaling_3d_scale, 0.15, 0.05)
			avg_fps += 5
#		elif vport.directional_shadow_atlas_size != 0:
#			vport.directional_shadow_atlas_size = vport.directional_shadow_atlas_size / 2 - 1
#			avg_fps += 15

	if avg_fps > 55:
		if vport.scaling_3d_scale < 1.0:
			move_toward(vport.scaling_3d_scale, 1.0, 0.05)
			avg_fps -= 5
#		elif vport.directional_shadow_atlas_size < 2048:
#			vport.directional_shadow_atlas_size = vport.directional_shadow_atlas_size * 2 + 1
#			avg_fps -= 15
		elif not $"../DirectionalLight3D".shadow_enabled:
			$"../DirectionalLight3D".shadow_enabled = true
			avg_fps -= 15
		elif not $"../WorldEnvironment".environment.glow_enabled:
			$"../WorldEnvironment".environment.glow_enabled = true
			avg_fps += 15
		elif not $PhysicsInterpolation/Trail.visible:
			$PhysicsInterpolation/Trail.show()
			avg_fps -= 15
		elif not $Dust.visible:
			$Dust.show()
			avg_fps -= 15

func _physics_process(delta):
	phys_time_prev = Time.get_ticks_msec()
	global_transform = global_transform.orthonormalized()

	var dir := Vector2.ZERO

	if Input.is_action_pressed("ui_up"):
		dir += Vector2.UP
	if Input.is_action_pressed("ui_down"):
		dir += Vector2.DOWN
	if Input.is_action_pressed("ui_left"):
		dir += Vector2.LEFT
	if Input.is_action_pressed("ui_right"):
		dir += Vector2.RIGHT

	dir = dir.normalized()

	var dir3D := Vector3(dir.y, 0, - dir.x)

	apply_torque_impulse(dir3D * speed * delta)


#	print("velocity: ", velocity)

	velocity = Vector2(linear_velocity.x, linear_velocity.z).length()
	avg_velocity = lerp(avg_velocity, velocity, 0.1)

#	print("avg_vel: ", avg_velocity)

#	var collisions = get_colliding_bodies()
#	print("collisions: ", collisions)
#	if collisions > previous_collisions:
#
#
#	previous_collisions = collisions
	#previous_velocity = velocity

	var spin = angular_velocity.length()
	avg_spin = lerp(avg_spin, spin, 0.5)

	$"../HUD/VBoxContainer2/Spin".text = "Spin: "+ str(round(spin))
#	print("spin: ", spin)

	$Spin.pitch_scale = smoothstep(0, 500, avg_spin) * 3 + 0.02
#	print("spin pitch: ", $Spin.pitch_scale)

	$Spin.volume_db = pow(smoothstep(0, 50, avg_spin), 0.15) * 100 - 60
#	print("spin unit_db: ", $Spin.unit_db)

var collision_force : Vector3 = Vector3.ZERO

func _integrate_forces(state : PhysicsDirectBodyState3D)->void:
	if not mobile:
		return
	collision_force = Vector3.ZERO
	for i in range(state.get_contact_count()):
		collision_force += state.get_contact_impulse(i) * state.get_contact_local_normal(i)
		var force = collision_force.length_squared()
		var last_collision_interval_usec = Time.get_ticks_usec() - time_of_last_collision_usec
		if force > 0.1 and last_collision_interval_usec >= 100000:
			print("Last collision was ", last_collision_interval_usec ," microseconds ago, of force: ", force)
			print("collision force: ", force)
			var volume = smoothstep(0.1, 50, force) * 3
			$Collision.unit_size = volume
			$PhysicsInterpolation/OmniLight3D.light_energy += smoothstep(0.1, 50, force) * 8
			$PhysicsInterpolation/MeshInstance3D.mesh.surface_get_material(0).emission_energy_multiplier += smoothstep(0.1, 30, force) * 1
			print("volume: ", volume)
			print("unit_size: ", $Collision.unit_size)
			$Collision.play()
			time_of_last_collision_usec = Time.get_ticks_usec()
		else:
			apply_central_impulse(- state.get_contact_impulse(i) / 1000)

func _process(delta) -> void:
	avg_fps = lerp(avg_fps, Engine.get_frames_per_second(), 0.05)

	$PhysicsInterpolation/OmniLight3D.light_energy = lerp($PhysicsInterpolation/OmniLight3D.light_energy, 1.0, pow(- 10.0 * delta, 2))
	$PhysicsInterpolation/MeshInstance3D.mesh.surface_get_material(0).emission_energy_multiplier = lerp($PhysicsInterpolation/MeshInstance3D.mesh.surface_get_material(0).emission_energy_multiplier, 0.0, pow(- 10.0 * delta, 2))

	$"../HUD/VBoxContainer2/Speed".text = "Speed: "+ str(round(velocity))
	$"../HUD/VBoxContainer2/Speed".text = "Speed: "+ str(round(velocity))

	$Speed.volume_db = pow(smoothstep(0, velocity_cap, avg_velocity), 0.25) * 90 - 85
	$Speed.pitch_scale = pow(smoothstep(0, velocity_cap, avg_velocity), 0.25) * 0.5 + 0.75

	#var xform_phys := global_transform
	#var xform_interp : Transform3D
	var cur_pos_int = $PhysicsInterpolation.global_position
	var cur_rot_int = $PhysicsInterpolation.global_rotation
	var pos_phys = global_position
	var rot_phys = global_rotation
	var pos_int : Vector3
	var rot_int : Vector3
	var weight = Engine.get_physics_interpolation_fraction()
	var vel_phys = linear_velocity
#	var pos_diff = pos_phys - pos_previous
	var pos_fut = lerp(pos_previous, pos_phys, 1.5)

#	print(weight)

	# a poor attempt at physics interpolation
	pos_int = cur_pos_int.lerp(pos_phys, weight + delta + pow(-48.0 * delta, 2))
	rot_int = cur_rot_int.lerp(rot_phys, weight + delta + pow(-48.0 * delta, 2))

	#pos_int = pos_int.cubic_interpolate_in_time(pos_phys, pos_previous, pos_fut, weight, -delta, 0, delta)

	$PhysicsInterpolation.global_position = pos_int
	$PhysicsInterpolation.global_rotation = rot_int

	pos_previous = global_position
	rot_previous = global_rotation

	$PhysicsInterpolation/Shadow.global_position = pos_int

	#$PhysicsInterpolation/OmniLight3D.light_energy = max($PhysicsInterpolation/OmniLight3D.light_energy - delta * 10000000, 0)

func reset():
#	mobile = false
	set_physics_process(false)
	set_process(false)
	freeze = true
	await get_tree().physics_frame

	linear_velocity = Vector3(0,-10,0)
	angular_velocity = Vector3.ZERO
	self.global_position = Vector3(0, 0.65, 0)
	pos_previous = Vector3(0, 0.65, 0)
	$PhysicsInterpolation.global_position = Vector3(0, 0.65, 0)
	$"../HUD".time = 0.0
	$"../HUD".active = false
	$Camera3D._ready()
	$"../HUD/VBoxContainer/PersonalBest".modulate = Color.WHITE
	$"../HUD/VBoxContainer/Time".modulate = Color.WHITE
	$"../CanvasLayer/Help/AnimationPlayer".play("RESET")

	await get_tree().create_timer(0.01).timeout.connect(func(): set_physics_process(true); set_process(true); mobile = true; freeze = false)

#func _on_Player_body_entered(body):
#	$Collision.play()

