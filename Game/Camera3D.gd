extends Camera3D

var shake_noise = FastNoiseLite.new()

@onready var target : RigidBody3D = get_parent()

var back_offset = 4
var forward_offset = 4.0
var up_offset = 4.0
var target_vel = Vector3.ZERO

@onready var look_target_pos = target.global_position

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	var target_pos = target.global_position
	var offset_back = Vector3.BACK * back_offset
	var offset_up = Vector3.UP * up_offset
	global_position = target_pos + offset_back + offset_up

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if not target.mobile:
		return

	var rate = pow(- 32.0 * delta, 2)
	var target_pos = target.global_position
	var target_vel_diff = target.linear_velocity - target_vel
	target_vel_diff.limit_length(0.5)
	target_vel = target_vel.lerp(target_vel + target_vel_diff, rate / 1 )

	var offset_back = Vector3.BACK * back_offset #* (5 - clamp(target_vel.length_squared() / 1000 ,1, 5))
	var offset_up = Vector3.UP * up_offset


	global_position = global_position.lerp(target_pos + offset_back + offset_up, rate)

	fov = 75 - smoothstep(0, 15, target_vel.length()) * 20

	look_target_pos = look_target_pos.lerp(Vector3.FORWARD * forward_offset + target_pos + target_vel.limit_length(15) * 0.2, rate * 2)

	var xform_looking_at = global_transform.looking_at(look_target_pos, Vector3.UP)

	global_transform = global_transform.interpolate_with(xform_looking_at, rate * 2)

	shake_noise.frequency = clamp(target_vel.length() / 1000, 1, 5)

	var offset_shake = Vector3(\
		shake_noise.get_noise_2d(Time.get_ticks_msec(), 0),
		shake_noise.get_noise_2d(Time.get_ticks_msec(), 2000),
		shake_noise.get_noise_2d(Time.get_ticks_msec(), 5000))
	var amount = target_vel.length_squared() / 150000

	global_translate(offset_shake * amount)
