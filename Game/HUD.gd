extends Control

var password = "DON'T CHEAT, YOU DONKEY! 2023-09-08"

var time := 0.0
var pb_time := 0.0

var active := false

func format_time(time, digit_format = "%02d"):
	var digits = []

	var minutes = digit_format % [time / 60]
	digits.append(minutes)

	var seconds = digit_format % [int(ceil(time)) % 60]
	digits.append(seconds)

	var hundreds = digit_format % [int(ceil(time * 100)) % 60]
	digits.append(hundreds)

	var formatted = String()
	var colon = ":"

	for digit in digits:
		formatted += digit + colon

	if not formatted.is_empty():
		formatted = formatted.rstrip(colon)

	return formatted

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	get_window().mode = Window.MODE_MAXIMIZED if (true) else Window.MODE_WINDOWED

	if not OS.has_feature("debug"):
		$FPS.hide()
#	var key = File.new()
#
#	key.open("user://key", File.READ)
#	if key.get_error() != 0:
#		randomize()
#		key.open("user://key", File.WRITE)
#		for _i in range(0, 31):
#			encryption_key.append(randi() % 255)
#		key.store_buffer(encryption_key)
#		key.close()
#	else:
#		encryption_key = key.get_buffer(31)
#		key.close()
#
#	print(var2str(encryption_key))
	if not FileAccess.file_exists("user://score"):
		return

	var score : FileAccess
	score = FileAccess.open_encrypted_with_pass("user://score", FileAccess.READ, password)#, encryption_key)
	if score.get_error() != 0: # can't read file
		$"VBoxContainer/PersonalBest".hide()
	else:
		pb_time = score.get_float()
		score.close()
		$"VBoxContainer/PersonalBest".text = "Personal Best: " + format_time(pb_time)
		$"VBoxContainer/PersonalBest".show()

#	if not OS.has_feature("debug"):
#	$HTTPRequest.request("https://unfa.xyz/s/Pei9zeeX/counter_gotm.php")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if active:
		time += delta
		if time > pb_time and pb_time != 0:
			$"VBoxContainer/Time".modulate = Color.RED
	$"VBoxContainer/Time".text = format_time(time)
#	$FPS.text = String(Engine.get_frames_per_second()) + " (" + String(round($"../Player".avg_fps)) + ") FPS"


func _on_Trigger_body_entered(body):
	if body.has_method("reset") and not active: # is this the player?
		active = true
		$"../Player/Go".play()
		create_tween().tween_property($"../CanvasLayer/Help", "modulate", Color(1,1,1, 0), 0.25).set_ease(Tween.EASE_OUT)\
		.finished.connect($"../CanvasLayer/Help".hide)


func _on_EndTrigger_body_entered(body):
	if body.has_method("reset") and active: # is this the player?
		active = false

		$"../Explosion".emitting = true
		$"../Explosion/Explosion".play()

		if time < pb_time or pb_time == 0.0: # new PB
			pb_time = time
			$"../Poggers/AnimationPlayer".play("Pooggers")
			$"VBoxContainer/PersonalBest".modulate = Color.GREEN
			$"../NewPBMusic".play()
			$"../Player/PB".play()
			var score : FileAccess
			score = FileAccess.open_encrypted_with_pass("user://score", FileAccess.WRITE, password)
			score.store_float(pb_time)
			score.close()

		else:
			$"../Player/Win".play()

		$"VBoxContainer/PersonalBest".show()
		$"VBoxContainer/PersonalBest".text = "Personal Best: " + format_time(pb_time)


func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	pass
#	if not FileAccess.file_exists("user://hitcount.gotm"):
#		return
#	var count : FileAccess
#	count = FileAccess.open("hitcount_gotm.php", FileAccess.READ)
#	var hits = int(count.get_as_text())
#
#	print("HTTP: ", [result, response_code, headers, body])
#
#	var suffix = " times" if hits > 1 else " time!" # YOU ARE FIRST! TAKE A SCREENSHOT!"
#	$HitCount.text = "this game was visited " + str(hits) + suffix
#	count.close()
#	if hits == 1:
#		$HitCount.modulate = Color(1.2, 0.25, 0.05)
